# **SECRET SANTA** #

## **How to get all the stuff on my linux powered server?** ##

* git clone https://timefcuk@bitbucket.org/timefcuk/santa.git
* virtualenv santa --python=python3.5
* cd santa
* source bin/activate
* pip install -r requirements.txt
* python santa.py

## **Goals**

* "**U**" means undone
* "**u**" means working at
* "**D**" means done

#### Tests 
* U - Write a list of tests that I have to create to fully test the application

#### Registration
* U - Add checkbox in group creating if the creator wants to participate in this group (and register him) 

#### Registration confirmation
* U - send a confirmation email for successful registration/creating group
* U - Add captcha into registration of new group and new user (and new present)

#### Presents
* U - add a link to presents creating page from group registring page
* U - It will be not a bad idea to give user a choice of presents after he registred - we could use his previous info about presents
* U - Add present_cost into group DB and print in in user.html

#### Styling 

* D - make redirect to registration link
* D - make user_reg face with input
* D - make user_reg validator
* D - make user_reg complete validation error report
* D - add presents creation and choose option for user_reg
* D - make user registration via santadb if data is validated 
* D - create shuffleGroup
* D - make autocleaner (since 200 days after giftdate) - deleting group and all tuples from UU that contain gifteruID same as uID from GU with gID from that group
* D - make createPresent(pID,Name,Description)
* D - make addPresent
* D - make data validator for create_group web page
* D - make complete error report if data was inputed not correct
* D - make creation of Group with santadb if all data is successfully validated
* D - return an error "there's registrated user with the same mail in this group" when user with the same mail is registred
* D - make getGifter from UU
* D - TEST RANDOM FUNCTION (AND ALL FROM USER/)
* D - add two /user/<> displays: 1st displays deadline 2nd displays person who user has to make gift to
* D - add presents to user/ page so people can see what presents their gifted has choosen
* D - make web interface with data validator

#### Known issues

* No module named _sqlite3 issue
You have to install `libsqlite3-dev` package and rebuild python3.5 with this package installed


This README would normally document whatever steps are necessary to get your application up and running.

#### What is this repository for?

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

#### How do I get set up?

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

#### Contribution guidelines

* Writing tests
* Code review
* Other guidelines

#### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
