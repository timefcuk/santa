from flask import *
import sqlite3
import datetime
import time
import subprocess
import os
import os.path
import random
from santadb import * 

app = Flask(__name__)

def symbolsValidation(word):
	bad_alphabet = [";","\""]
	for i in range(0,len(word)):
		for j in bad_alphabet:
			if word[i]==j:
				return False
	return True

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# ||			GROUP REGISTRATION              ||
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
@app.route('/', methods=['GET', 'POST'])
def create_group():
	error = None
	if request.method == 'POST':
		Name = request.form['Name']
		Description = request.form['Description']
		regYear = request.form['regYear']
		regMonth= request.form['regMonth']
		regDay = request.form['regDay']
		giftYear = request.form['giftYear']
		giftMonth = request.form['giftMonth']
		giftDay = request.form['giftDay']
		""" ERRORS """
		errorlist = ""
		if not symbolsValidation(Name):
			errorlist+="Name contains unpropriate characters, "
		if not symbolsValidation(Description):
			errorlist+="Description contains unpropriate characters, "
		if not symbolsValidation(regYear):
			errorlist+="Registration year contains unpropriate characters, "
		if not symbolsValidation(regMonth):
			errorlist+="Registration month contains unpropriate characters, "
		if not symbolsValidation(regDay):
			errorlist+="Registration day contains unpropriate characters"
		if not symbolsValidation(giftYear):
			errorlist+="Gift year contains unpropriate characters, "
		if not symbolsValidation(giftMonth):
			errorlist+="Gift month contains unpropriate characters, "
		if not symbolsValidation(giftDay):
			errorlist+="Gift day contains unpropriate characters, "
		try:	
			if datetime.datetime(int(regYear),int(regMonth),int(regDay))<datetime.datetime.now():
# *******************************************
				errorlist+="Registration deadline date has to be greater than "+datetime.datetime.now().strftime("%Y-%m-%d")+", "
				print()
#*******************************************
			if datetime.datetime(int(regYear),int(regMonth),int(regDay))>datetime.datetime(int(giftYear),int(giftMonth),int(giftDay)):
				errorlist+="Gift deadline date has to be greater than registration deadline date, "
		except Exception as e:
			return render_template("create_group.html",error=e)	

		if errorlist!="":
			return render_template("create_group.html",error=errorlist)
		""" ERRORS """

		if errorlist=="":
			regDeadline = datetime.datetime(int(regYear),
											int(regMonth),
											int(regDay))
			giftDeadline = datetime.datetime(int(giftYear),
											 int(giftMonth),
											 int(giftDay))
			A = Santa_db.createGroup(Name,
									 regDeadline,
									 giftDeadline,
									 Description)
			if A==False:
				return render_template("create_group.html",
										error="DB_ERROR")
			return render_template("success_group.html",
									group_code=A)	
	return render_template("create_group.html",
							error = False)

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# ||		USER REGISTRATION			   ||
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
@app.route('/group/<group_code>',methods=['GET','POST'])
def register_user(group_code):
	regDeadline = Santa_db.getGroupDeadline(group_code)
	rDl = datetime.datetime.strptime(regDeadline,"%Y-%m-%d")
# *************************************
	
	if rDl < datetime.datetime.now():
		return render_template("register_user.html",
								group_name = Santa_db.getGroupName(group_code),
								group_code=group_code,
								Description = Santa_db.getGroupDescription(group_code),
								Presents = Santa_db.getPresents(), 
								error = "Sorry, but registration is finished")
	
# *************************************
	if request.method == 'POST':
		Mail = request.form['Mail']
		Fst_name = request.form['Fst_name']
		Snd_name = request.form['Snd_name']
		errorlist = ""
		if Mail=="":
			errorlist+="Mail is empty,"
		if Fst_name=="":
			errorlist+="First name is empty, "
		if Snd_name=="":
			errorlist+="Second name is empty, "
		if not symbolsValidation(Mail):
			errorlist+="Uncorrect symbols in your mail, "
		if not symbolsValidation(Fst_name):
			errorlist+="Uncorrect symbols in your first name, "
		if not symbolsValidation(Snd_name):
			errorlist+="Uncorrect symbols in your second name, "
		if Santa_db.mailRegistred(Mail,group_code):
			errorlist+="This Mail has been registred in this group, "
		if errorlist!="":
			return render_template("register_user.html",
									group_name = Santa_db.getGroupName(group_code),
									group_code=group_code,
									Description = Santa_db.getGroupDescription(group_code),
									Presents = Santa_db.getPresents(), 
									error=errorlist)

		USER = Santa_db.createUser(Mail,Fst_name,Snd_name,group_code)
		
		Present_list =  Santa_db.getPresents()
		for i in Present_list:
			try:
				a = i[0]
				chosenPresent = request.form[str(i[0])+"_present"]
				present_add = Santa_db.addPresent(USER,i[0])
				if not present_add:
					return render_template("register_user.html",
											group_name = Santa_db.getGroupName(group_code),
											group_code=group_code,
											Description = Santa_db.getGroupDescription(group_code),
											error="error with presents - refuse of choosing ones")
			except KeyError:
				a = 0
		
		regDeadline = Santa_db.getGroupDeadline(group_code)
		rDl = datetime.datetime.strptime(regDeadline,"%Y-%m-%d")
		SrDL = rDl.strftime("%Y-%m-%d")
		return render_template("success_user.html",
								reg_deadline=SrDL,
								user_code=USER)
	return render_template("register_user.html",
							group_name = Santa_db.getGroupName(group_code),
							group_code=group_code,
							Presents = Santa_db.getPresents(),
							Description = Santa_db.getGroupDescription(group_code))

"""app.route('/presents/<user_code>',methods=['GET','POST'])
def add_presents(user_code):
	uID = user_code"""
	

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# ||		GETTING GIFTED PERSON NAME      ||
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
@app.route('/user/<user_code>',methods=['GET'])
def user(user_code):
	gID = Santa_db.getUserGroup(user_code)
	regDeadline = Santa_db.getGroupDeadline(gID)
	rDl = datetime.datetime.strptime(regDeadline,"%Y-%m-%d")
	if datetime.datetime.now()<rDl:
		return render_template("user.html",
								Deadline = rDl.strftime("%Y-%m-%d"))
	else:
		if Santa_db.isShuffled(gID)==False:
			Santa_db.shuffleGroup(gID)
		Gifted_Names=Santa_db.getGifted(user_code)
		Presents_chosen = Santa_db.getUserPresents(user_code)
		return render_template("user.html",
								Gifted=Gifted_Names,
								Presents = Presents_chosen)


# getDeadline OK-> shuffled? OK-> return gifted
# getDeadline NO-> return regDeadline
# getDeadline OK-> shuffled? NO -> shuffle -> return gifted

if __name__ == "__main__":
	Santa_db = Santa()	
	app.run(host='0.0.0.0',port=5000,debug=True)

