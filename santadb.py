import sqlite3
import datetime
import time
import subprocess
import os
import os.path
import random


__name__ = "santadb"
"""alphabet_info: 		
0..9 = 48..57
A..Z = 65..90
a..z = 97..122
"""

class Santa:
	santa_folder ="santadb/" # DO NOT CHANGE THIS PATH - BASH SCRIPT USES IT!
	alphabet = []
	def __init__(self):	
		if (os.path.isfile(self.santa_folder+"santa.db")) == False:
			random.seed(None)
			subprocess.run(["mkdir",self.santa_folder])
			subprocess.run(["touch",self.santa_folder+"santa.db"])
			subprocess.run(["sh","read.sh"])
		for i in range(48,58):
			self.alphabet.append(chr(i))
		for i in range(65,91):
			self.alphabet.append(chr(i))
		for i in range(97,123):
			self.alphabet.append(chr(i))
		

	def genKey(self):		
		key = ""
		key_length = random.randint(15,40)
		for i in range(0,key_length):
			new_symbol = random.randint(0,len(self.alphabet)-1)
			key+=self.alphabet[new_symbol]
		return key


	def validateGroupKey(self,key):
		con = sqlite3.connect(self.santa_folder+"santa.db")
		cur = con.cursor()
		cur.execute("SELECT gID FROM Groups")
		keys = cur.fetchall()
		con.commit()
		cur.close()
		for i in keys:
			if key == i[0]:
				return False
		return True


	def createGroupKey(self):
		key = self.genKey()
		while (self.validateGroupKey(key)!=True):
			key = self.genKey()
		return key
		
	def createGroup(self,Name,regDeadline,giftDeadline,Desсription):
		self.autocleaner()
		con = sqlite3.connect(self.santa_folder+"santa.db")
		cur = con.cursor()
		cur.execute("PRAGMA foreign_keys=ON")
		try:
			gID = self.createGroupKey()
			regDeadline = regDeadline.strftime("%Y-%m-%d")
			giftDeadline = giftDeadline.strftime("%Y-%m-%d")
			cur.execute("INSERT INTO Groups values(?,?,?,?,?)",(gID,Name,regDeadline,giftDeadline,Desсription))
			con.commit()
			cur.close()
			return gID
		except sqlite3.Error as er:
			print("error from createGroupKey: {0}".format(er))
			con.rollback()
			cur.close()
			return False

	def validateUid(self,key):
		try:
			con = sqlite3.connect(self.santa_folder+"santa.db")
			cur = con.cursor()
			cur.execute("SELECT uID FROM UR")
			keys = cur.fetchall()
			con.commit()
			cur.close()
			for i in keys:
				if key == i[0]:
					return False
			return True
		except sqlite3.Error as er:
			print("error from validateUid: {0}".format(er))
			return False
	
	def createUid(self):
		key = self.genKey()
		while (self.validateUid(key)!=True):
			key = self.genKey()
		return key

	def createUser(self,Mail,Fst_name,Snd_name,gID):
		con = sqlite3.connect(self.santa_folder+"santa.db")
		cur = con.cursor()
		try:
			cur.execute("PRAGMA foreign_keys=ON")

			uID = self.createUid()
			
			cur.execute("SELECT * FROM UR join GU on (UR.uID=GU.uID) WHERE Mail=? and gID=?",(Mail,gID))
			confirm = cur.fetchall()
			if confirm==[]:
				cur.execute("SELECT * FROM User WHERE Mail = ?",(Mail,))
				confirm = cur.fetchall()
				if confirm==[]:
					cur.execute("INSERT INTO User values(?,?,?)",(Mail,Fst_name,Snd_name))
				else:
					cur.execute("UPDATE User SET Fst_name = ?, Snd_name = ? WHERE Mail = ?",(Fst_name,Snd_name,Mail))
				cur.execute("INSERT INTO UR values(?,?)",(Mail,uID))
				cur.execute("INSERT INTO GU values(?,?)",(gID,uID))
	
				con.commit()
				cur.close()
				return uID
			else:
				return False
		except sqlite3.Error as er:
			print("error from createUser: {0}".format(er))
			con.rollback()
			cur.close()
			return False

	def shuffleGroup(self,gID):
		con = sqlite3.connect(self.santa_folder+"santa.db")
		cur = con.cursor()
		try:
			cur.execute("PRAGMA foreign_keys=ON")
			cur.execute("SELECT uID from GU WHERE gID = ?",(gID,))
			confirm = cur.fetchall()
			if confirm==[]:
				return False
			cur.execute("SELECT gifter_uID FROM UU WHERE gifter_uID IN (SELECT uID FROM GU WHERE gID =?)",(gID,))
			confirm = cur.fetchall()
			if confirm!=[]:
				return False
			cur.execute("SELECT uID FROM GU WHERE gID=?",(gID,))
			_uidList = cur.fetchall() #raw uID list with 2D array
			uidList = []
			for i in _uidList:
				uidList.append(i[0])
			for i in range(0,5):
				random.shuffle(uidList)
			for i in range(0,len(uidList)):
				if i==len(uidList)-1:
					cur.execute("INSERT INTO UU values(?,?)",(uidList[i],uidList[0]))
				else:
					cur.execute("INSERT INTO UU values(?,?)",(uidList[i],uidList[i+1]))
			con.commit()
			cur.close()
			return True
		except sqlite3.Error as er:
			print("error from shuffleGroup: {0}".format(er))
			con.rollback()
			cur.close()
			return False

	def createPresent(self,Name,Description):
		con = sqlite3.connect(self.santa_folder+"santa.db")
		cur = con.cursor()
		
		try:
			cur.execute("PRAGMA foreign_keys=ON")

			cur.execute("INSERT INTO Presents(Name,Description) values(?,?)",(Name,Description))
			con.commit()
			cur.close()
			return True
		except sqlite3.Error as er:
			print("error from createPresent: {0}".format(er))
			con.rollback()
			cur.close()
			return False

	def addPresent(self,uID,pID):
		con = sqlite3.connect(self.santa_folder+"santa.db")
		cur = con.cursor()	
		try:
			cur.execute("PRAGMA foreign_keys=ON")
			cur.execute("INSERT INTO PU values(?,?)",(uID,pID))
			con.commit()
			cur.close()
			return True
		except sqlite3.Error as er:
			print("error from addPresent: {0}".format(er))
			con.rollback()
			cur.close()
			return False
	
	def getGroupName(self,gID):
		con = sqlite3.connect(self.santa_folder+"santa.db")
		cur = con.cursor()
		try:
			cur.execute("SELECT Name FROM Groups WHERE gID = ?",(gID,))
			gName = cur.fetchall()
			con.commit()
			cur.close()
			return gName[0][0]
		except sqlite3.Error as er:
			print("error from getGroupName: {0}".format(er))
			con.rollback()
			cur.close()
			return False

	def getGroupDescription(self,gID):
		con = sqlite3.connect(self.santa_folder+"santa.db")
		cur = con.cursor()
		try:
			cur.execute("SELECT Description FROM Groups WHERE gID = ?",(gID,))
			gDescription = cur.fetchall()
			con.commit()
			cur.close()
			return gDescription[0][0]
		except sqlite3.Error as er:
			print("error from getGroupDescription: {0}".format(er))
			con.rollback()
			cur.close()
			return False

	def autocleaner(self):
		con = sqlite3.connect(self.santa_folder+"santa.db")
		cur = con.cursor()
		cur.execute("PRAGMA foreign_keys=ON")
		try:
			cur.execute("DELETE FROM UU WHERE gifter_uID in (select uID from GU where gID in (select gID from Groups where julianday('now')-julianday(giftDeadline) > 200))")
			cur.execute("DELETE FROM PU WHERE uID in (select uID from GU where gID in (select gID from Groups where julianday('now')-julianday(giftDeadline) > 200))")
			cur.execute("DELETE FROM GU WHERE gID in (select gID from Groups where julianday('now')-julianday(giftDeadline) > 200)")
			cur.execute("DELETE FROM UR WHERE uID not in (select uID from GU)")
			cur.execute("DELETE FROM Groups WHERE julianday('now')-julianday(giftDeadline) > 200")
			con.commit()
			cur.close()
			return True
		except sqlite3.Error as er:
			print("error from autocleaner: {0}".format(er))
			con.rollback()
			cur.close()
			return False

# get all presents from DB
	def getPresents(self):
		con = sqlite3.connect(self.santa_folder+"santa.db")
		cur = con.cursor()
		try:
			cur.execute("SELECT * FROM Presents")
			Presents = cur.fetchall()
			con.commit()
			cur.close()
			return Presents
		except sqlite3.Error as er:
			print("error from getPresents from db: {0}".format(er))
			con.rollback()
			cur.close()
			return False

	def getUserPresents(self,uID):
		con = sqlite3.connect(self.santa_folder+"santa.db")
		cur = con.cursor()
		try:
			cur.execute("SELECT * FROM Presents WHERE pID in (SELECT pID FROM PU WHERE uID = (select gifted_uID from UU where gifter_uID = ?))",(uID,))
			Presents = cur.fetchall()
			con.commit()
			cur.close()
			return Presents
		except sqlite3.Error as er:
			print("error from getPresents for user: {0}".format(er))
			con.rollback()
			cur.close()
			return False

	def getGroupDeadline(self,gID):
		con = sqlite3.connect(self.santa_folder+"santa.db")
		cur = con.cursor()
		try:
			cur.execute("SELECT regDeadline FROM Groups where gID = ?",(gID,))
			Deadline = cur.fetchall()[0][0]
			
			con.commit()
			cur.close()
			return Deadline
		except sqlite3.Error as er:
			print("error from getGroupDeadline: {0}".format(er))
			con.rollback()
			cur.close()
			return False

	def mailRegistred(self,Mail,gID):
		con = sqlite3.connect(self.santa_folder+"santa.db")
		cur = con.cursor()
		try:
			cur.execute("SELECT * FROM User NATURAL JOIN UR NATURAL JOIN GU WHERE Mail = ? and gID = ?",(Mail, gID))
			ans = cur.fetchall()
			con.commit()
			cur.close()
			if ans == []:
				return False
			return True
		except sqlite3.Error as er:
			print("error from mailRegistred: {0}".format(er))
			con.rollback()
			cur.close()
			return False

	def getGifted(self,uID):
		con = sqlite3.connect(self.santa_folder+"santa.db")
		cur = con.cursor()
		try:
			cur.execute("select Fst_name,Snd_name from User where Mail in (select Mail from UR where uID in (select gifted_uID from UU where gifter_uID = ?)) ",(uID,))
			ans = cur.fetchall()
			con.commit()
			cur.close()
			return ans[0]	
		except sqlite3.Error as er:
			print("error from mailRegistred: {0}".format(er))
			con.rollback()
			cur.close()
			return False

	def getGifteduID(self,uID):
		con = sqlite3.connect(self.santa_folder+"santa.db")
		cur = con.cursor()
		try:
			cur.execute("select gifted_uID from UU where gifter_uID = ?",(uID,))
			ans = cur.fetchall()
			con.commit()
			cur.close()
			return ans[0]	
		except sqlite3.Error as er:
			print("error from mailRegistred: {0}".format(er))
			con.rollback()
			cur.close()
			return False

	def getUserGroup(self,uID):
		con = sqlite3.connect(self.santa_folder+"santa.db")
		cur = con.cursor()
		try:
			cur.execute("SELECT gID FROM GU WHERE uID = ?",(uID,))
			ans = cur.fetchall()
			con.commit()
			cur.close()
			return ans[0][0]	
		except sqlite3.Error as er:
			print("error from getUserGroup: {0}".format(er))
			con.rollback()
			cur.close()
			return False
	
	def isShuffled(self,gID):
		con = sqlite3.connect(self.santa_folder+"santa.db")
		cur = con.cursor()
		try:
			cur.execute("SELECT * FROM UU WHERE gifter_uID IN (SELECT uID FROM GU WHERE gID = ?) ",(gID,))
			ans = cur.fetchall()
			con.commit()
			cur.close()
			if ans==[]:
				return False
			else:
				return True	
		except sqlite3.Error as er:
			print("error from isShuffled: {0}".format(er))
			con.rollback()
			cur.close()
			return False


"""	
TEST = Santa()
a = TEST.createGroup("Santa Claus",datetime.datetime.now()+datetime.timedelta(days=250),datetime.datetime.now()+datetime.timedelta(days=300),"Some description")

#TEST.createUser("anna@gmail.com","Anna","Fomina")

print(TEST.createUser("manna@gmail.com","Anna","Fomina",a))
print(TEST.createUser("andr@gmail.com","Andrey","Poprygin",a))
print(TEST.createUser("jaja@gmail.com","Jaja","Magaga",a))
b = TEST.createUser("anuta@gmail.com","Anuta","Magaga",a)
print(TEST.createPresent("Candy","Simple candy made from caramel"))
print(TEST.addPresent(b,1))

#print(TEST.shuffleGroup("2PV6anI4u99PeffqVNILCrBD4nFIiYdI"))

#print(TEST.autocleaner())
"""

